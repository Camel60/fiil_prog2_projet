#include <stddef.h>

/*@ requires \valid(s + (0 .. length - 1));
    requires \valid(stack + (0 .. length - 1));
    requires \valid(right + (0 .. length - 1));
    requires \separated(stack + (0 .. length - 1),right + (0 .. length - 1));
    requires \separated(stack + (0 .. length - 1), s + (0 .. length - 1));
    requires \separated(right + (0 .. length - 1), s + (0 .. length - 1));

    assigns stack[0 .. length - 1], right[0 .. length - 1];

    ensures wf_right: 
      \forall integer i; 1 <= i <= length 
      ==> i+1 < right[i] <= length+1;
    ensures right_small:
      \forall integer i; 1 <= i <= length
      ==> right[i-1] != length+1
      ==> s[right[i-1]-1] < s[i-1];
    ensures right_smallest:
      \forall integer i; 1 <= i <= length 
      ==> \forall integer j; i <= j < right[i-1]
      ==> s[j-1] >= s[i-1];
*/
void neighbor_right(int* s, size_t length, size_t* stack, size_t* right) {
  size_t sidx = 0;
  size_t x = length-1;
  /*@
    loop invariant s_untouched:
			\forall integer idx; 0 <= idx < length 
			==> s[idx] == \at(s[idx],Pre);
    loop invariant x_bounded:
			0 <= x <= length;
    loop invariant sidx_bounded:
			0 <= sidx <= x;
    loop invariant stack_right:
      \forall integer i; 0 <= i < sidx 
      ==> x+1 < stack[i] <= length+1;
    loop invariant wf_right: 
      \forall integer i; 1 <= i <= x+1 
      ==> i+1 < right[i] <= length+1;
    loop invariant right_small:
      \forall integer i; 1 <= i <= x+1
      ==> right[i-1] != length+1
      ==> s[right[i-1]-1] < s[i-1];
    loop invariant right_smallest:
      \forall integer i; 1 <= i <= x+1 
      ==> \forall integer j; i <= j < right[i-1]
      ==> s[j-1] >= s[i-1];
  	loop invariant stack_order:
      \forall integer i, j; 0 <= i < j < sidx 
      ==> stack[i] > stack[j] >= length+1;
  	loop invariant stack_sorder:
      \forall integer i, j; 0 <= i < j < sidx 
      ==> s[stack[i]-1] < s[stack[j]-1];
  	loop invariant s_begin:
      sidx > 0 
      ==> \forall integer i; stack[0]-1 <= i <= length-1 
      ==> s[i] >= s[stack[0] - 1];
  	loop invariant step_n:
      x < length-1 
      ==> sidx > 0 && stack[sidx - 1] == x;
  	loop invariant stack_summary:
      \forall integer i; 0 <= i < sidx - 1 
      ==> \forall integer j; stack[i] <= j < stack[i+1]-1 
      ==> s[j] >= s[stack[i+1]-1];
  	loop invariant stack_push: 
      sidx > 0 
      ==> stack[sidx-1] == x;
    loop assigns x, y, sidx, stack[0 .. length - 1], right[0 .. length - 1];
    loop variant length - y;
   */
  for (size_t y = 0; y < length; y++) {
    x = length-1-y; 
    /*@
    	loop invariant s_untouched_inner:
      \forall integer idx; 0 <= idx < length 
        ==> s[idx] == \at(s[idx],Pre);
    	loop invariant 0 <= sidx <= \at(sidx,LoopEntry);
    	loop invariant right_bigger:
        sidx > 0 
        ==> \forall integer i; x < i < stack[sidx-1]
        ==> s[i] >= s[x];
    	loop invariant stack_empty:
        sidx == 0 
        ==> \forall integer i; x < i < length 
        ==> s[i] >= s[x];
    	loop assigns sidx;
    	loop variant sidx;
    */
    while (sidx > 0 && s[stack[sidx-1]-1] >= s[x]) sidx--;
    if (sidx == 0) {
    right[x] = length+1;
    } else {
    /*@ 
    	assert head_ok:
        \forall integer i; x < i < stack[sidx-1] 
        ==> s[i] >= s[x];
    */
    right[x] = stack[sidx - 1];
    }
    /*@ 
    	assert a1: 
        right[x] != length+1 ==> s[right[x] - 1] < s[x];
    */
label:
    stack[sidx] = x + 1;
    /*@ 
    	assert s_untouched:
        \forall integer idx; 0 <= idx < length 
        ==> s[idx] == \at(s[idx],Pre);
    */
    //@ assert same: right[x] == \at(right[x], label);
    //@ assert a2: right[x] != length+1 ==> s[right[x] - 1] < s[x];
    sidx++;
  }
}